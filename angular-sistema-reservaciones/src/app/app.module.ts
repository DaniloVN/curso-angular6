import { BrowserModule } from '@angular/platform-browser'; 
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { IngresoReservacionComponent } from './ingreso-reservacion/ingreso-reservacion.component';
import { ListaReservacionesComponent } from './lista-reservaciones/lista-reservaciones.component';

@NgModule({
  declarations: [
    AppComponent,
    IngresoReservacionComponent,
    ListaReservacionesComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
