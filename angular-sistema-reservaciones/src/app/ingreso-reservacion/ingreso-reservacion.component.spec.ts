import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IngresoReservacionComponent } from './ingreso-reservacion.component';

describe('IngresoReservacionComponent', () => {
  let component: IngresoReservacionComponent;
  let fixture: ComponentFixture<IngresoReservacionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IngresoReservacionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IngresoReservacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
