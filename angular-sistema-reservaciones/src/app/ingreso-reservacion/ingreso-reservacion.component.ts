import { Component, OnInit } from '@angular/core';
import { Reservacion } from '../model/Reservacion.model';

@Component({
  selector: 'app-ingreso-reservacion',
  templateUrl: './ingreso-reservacion.component.html',
  styleUrls: ['./ingreso-reservacion.component.css']
})
export class IngresoReservacionComponent implements OnInit {
  idReservacion: number;
  reservaciones: Reservacion[];
  constructor() { 
    this.reservaciones = [];
    this.idReservacion = 1;
  }

  ngOnInit() {
  }

  /**
   * 
   * @param nombreHotel 
   * @param numeroHabitaciones 
   * @param cantidadPersonas 
   * @param fechaReservacion 
   */
  crearReservacion(nombreHotel:string, numeroHabitaciones:number, cantidadPersonas:number, 
    fechaReservacion:string):boolean {
    this.reservaciones.push(new Reservacion(this.idReservacion,nombreHotel,fechaReservacion,
      numeroHabitaciones,cantidadPersonas));
    this.idReservacion ++;
    return false;
  }

}
