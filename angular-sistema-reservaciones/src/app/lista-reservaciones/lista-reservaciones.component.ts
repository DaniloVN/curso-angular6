import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { Reservacion } from '../model/Reservacion.model';

@Component({
  selector: 'app-lista-reservaciones',
  templateUrl: './lista-reservaciones.component.html',
  styleUrls: ['./lista-reservaciones.component.css']
})
export class ListaReservacionesComponent implements OnInit {
  @Input() reservacion: Reservacion;
  @HostBinding('attr.class') cssClass = 'col-md-4';
  constructor() { }

  ngOnInit() {
  }

}
