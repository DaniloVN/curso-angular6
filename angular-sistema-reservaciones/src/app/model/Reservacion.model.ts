export class Reservacion {
    idReservacion: number;
    nombreHotel: string;
    fechaReservacion: string;
    numeroHabitaciones: number;
    cantidadPersonas: number;

    constructor($idReservacion: number, $nombreHotel: string, $fechaReservacion: string, $numeroHabitaciones: number,
         $cantidadPersonas: number) {
		this.idReservacion = $idReservacion;
		this.nombreHotel = $nombreHotel;
		this.fechaReservacion = $fechaReservacion;
		this.numeroHabitaciones = $numeroHabitaciones;
		this.cantidadPersonas = $cantidadPersonas;
	}
}