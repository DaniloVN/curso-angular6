import {Injectable} from '@angular/core';
import { DestinoViaje } from './destino-viaje.model';
import { tap, last } from 'rxjs/operators';
import { Action } from '@ngrx/store';
import { Store } from '@ngrx/store';
import {
		DestinosViajesState,
		NuevoDestinoAction,
		ElegidoFavoritoAction,
		EliminarDestinoAction
	} from './destinos-viajes-states.model';
import {AppState, db} from './../app.module';
import { HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { ConditionalExpr } from '@angular/compiler';

@Injectable()
export class DestinosApiClient {
	destinos:DestinoViaje[]=[];

	constructor(private store: Store<AppState>) {
		this.store
			.select(state => state.destinos)
			.subscribe((data) => {
				this.destinos = data.items;
			});
		this.store
			.subscribe((data) => {
			});
	}
	
	add(d:DestinoViaje){
	//   this.store.dispatch(new NuevoDestinoAction(d));
	// const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN' : 'token-seguridad'});
	// const req = new HttpRequest('POST', this.config.apiEndPoint + '/my', {nuevo: d.nombre} , {headers:headers});
	// this.http.request(req).subscribe((data: HttpResponse<{}>) => {
	// 	if (data.status === 200) {
	// 		this.store.dispatch(new NuevoDestinoAction(d));
	// 		const myDb = db;
	// 		myDb.destinos.add(d);
	// 		console.log('todos los destinos de la db!');
	// 		myDb.destinos.toArray().then(destinos => console.log(destinos));
	// 	}
	// })
	}
	getById(id:String):DestinoViaje{
	  return this.destinos.filter(function(d){return d.id.toString() == id;})[0];
	}
	getAll():DestinoViaje[] {
		return this.destinos;
	}
	elegir(d:DestinoViaje){
	this.store.dispatch(new ElegidoFavoritoAction(d));
	}
	deleteOne(idx:number){
		// this.store.dispatch(new EliminarDestinoAction());
	}
}