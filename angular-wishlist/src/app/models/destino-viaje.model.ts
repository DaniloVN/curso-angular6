export class DestinoViaje {
    private selected: boolean;
    servicios: string[];
    id: number;
    
    constructor(public nombre:string,  public u:string, public votes:number = 0) {  
        this.servicios = ['pileta', 'desayuno'];
    }
    
    isSelected(): boolean {
        return this.selected;
    }

    setSelected(value:boolean) {
        this.selected = value;
    }

    voteUp() {
        this.votes++;
    }

    voteDown() {
        this.votes--;
    }
}
